import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.Security;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SubjectTerm;

public class SignInPage {

    @FindBy(xpath = "//div[@placeholder='Choose Language']")
    WebElement languageDropDown;

    @FindBy(xpath = "//div[@id='ui-select-choices-row-1-0']//a//div[@ng-bind-html='language']")
    WebElement EnglishLanguage;

    @FindBy(xpath = "//div[@id='ui-select-choices-row-1-1']//a//div[@ng-bind-html='language']")
    WebElement DutchLanguage;

    @FindBy(xpath = "//a[text()='Register']")
    WebElement registerButton;

    @FindBy(xpath = "//input[@id='name']")
    WebElement username;

    @FindBy(xpath = "//input[@id='orgName']")
    WebElement organisationName;

    @FindBy(xpath = "//input[@id='singUpEmail']")
    WebElement emailName;

    @FindBy(xpath = "//span[text()='I agree to the']")
    WebElement iAgreeButton;

    @FindBy(xpath = "//button[text()='Get Started']")
    WebElement getStartButton;


    WebDriver driver;

    public SignInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void validateDropDown() {
        WebDriverWait wait = new WebDriverWait(driver, 10000);
        wait.until(ExpectedConditions.elementToBeClickable(languageDropDown));
        //languageDropDown.isDisplayed();
        languageDropDown.click();
        String englishText = EnglishLanguage.getText();
        String dutchText = DutchLanguage.getText();

        if (englishText.equalsIgnoreCase("English"))
            System.out.println("DropDown is available with Language as English");
        else
            System.out.println("DropDown is NOT available with Language as English");

        if (dutchText.equalsIgnoreCase("Dutch"))
            System.out.println("DropDown is available with Language as Dutch");
        else
            System.out.println("DropDown is NOT available with Language as Dutch");

        EnglishLanguage.click();
    }

    public void enterRegisterPage() {
        registerButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 10000);
        wait.until(ExpectedConditions.elementToBeClickable(username));
    }

    public void enterRegistrationDetail(String userName, String orgName, String email) {
        username.sendKeys(userName);
        organisationName.sendKeys(orgName);
        emailName.sendKeys(email);
        iAgreeButton.click();
        getStartButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 15000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()=' A welcome email has been sent. Please check your email. ']")));
    }

    public void GmailUtils(String username, String password) throws Exception {
        System.out.println("Email Verification Started !!!");
        String to = username;
//        final String username = "agann5198@gmail.com";
//        final String password = "Agan@1234";
        String host = "smtp.gmail.com";

        Properties props = new Properties();

        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.user", "agann5198@gmail.com");
        props.put("mail.smtp.password", "Agan@1234");
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.starttls.required", true);
        props.put("mail.smtp.startssl.enable", true);
        props.put("mail.smtp.startssl.required", true);

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        session.setDebug(true);

        Store store = session.getStore("imaps");
        store.connect("imap.gmail.com", "agann5198@gmail.com", "Agan@1234");

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_WRITE);

        System.out.println("Total Message:" + folder.getMessageCount());
        System.out.println("Unread Message:" + folder.getUnreadMessageCount());

        Message[] messages = null;
        boolean isMailFound = false;
        Message mailFromGod = null;
        for (int i = 0; i < 5; i++) {
            messages = folder.search(new SubjectTerm("Hi rahulPrabu - Please Complete JabaTalks Account"), folder.getMessages());
            if (messages.length == 0) {
                Thread.sleep(10000);
            }
        }
        for (Message mail : messages) {
            if (!mail.isSet(Flags.Flag.SEEN)) {
                mailFromGod = mail;
                System.out.println("Message Count is: " + mailFromGod.getMessageNumber());
                isMailFound = true;
            }
        }
        if (!isMailFound) {
            System.out.println("Could not find new mail from God :-(");
        }
        if (isMailFound)
            System.out.println("EMAIL Present !!!");
        else
            System.out.println("EMAIL NOT Present !!!");
    }

    public String getProjectPath() {
        String projectPath = System.getProperty("user.dir").replace("\\", "\\\\");
        return projectPath;
    }


}
