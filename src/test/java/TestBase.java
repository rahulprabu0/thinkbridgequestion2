import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class TestBase {

    WebDriver driver;
    SignInPage page;
    public Properties properties = new Properties();
    String username;
    String emailID;
    String organisationName;
    String password;
    String URL;

    @BeforeMethod
    public void initiateDriver() throws FileNotFoundException {
        System.out.println("----------------Test Begins----------------");

        System.out.println("## Initiated Chrome Driver");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        page = new SignInPage(driver);

        System.out.println("## Initiated Getting Data from Property File");
        String propFileName = "config.properties";
        InputStream inputStream = new FileInputStream(new File(page.getProjectPath() + "\\src\\main\\resources\\" + propFileName));

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testScenario() throws Exception {

        URL = properties.getProperty("URL");
        username = properties.getProperty("Username");
        organisationName = properties.getProperty("OrganisationName");
        emailID = properties.getProperty("EmailID");
        password = properties.getProperty("Password");

        driver.get(URL);
        page.validateDropDown();
        page.enterRegisterPage();
        page.enterRegistrationDetail(username, organisationName, emailID);
        page.GmailUtils(emailID, password);
    }
}

