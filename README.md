# ThinkBridgeQuestion2

I have implemented Page Object Model Framework with TestNG as Runner Libary supporter in this framework.

Used WebDriverManager Library files to reduce the dependency on importing webdriver exe into the project and updating them as per the browser version changes we have used WebDriverManager to make framework more robust.

Framework is DATA-DRIVEN type where data are extracted from Config.Properties file

In most scenarios added Explicit waits to increase performance of Suite.

For Email verification used SMTP server validation with Javax.mail-API and looped inside the UNSEEN messages are fulfilled the verification.

STEPS TO RUN SCRIPT:

1- Enter the Username and Password at Config.Properties file at C:\Resp\ThinkBridgeQuestion2\src\main\resources.

2- Run testScenario() method at TestBase.java file at C:\Resp\ThinkBridgeQuestion2\src\test\java

POINTS TO BE NOTED:

1- Email Verification is applicable only for GMAIL Accounts and GMAIL account should be Less Security Enabled.

2- Enter only GMAIL account with public imaps and SMTP server hosted. 

